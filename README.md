#  Gatsby + React + Netlify + Contentful w/ Webhook based Portfolio

https://portfolio.jeremyabrahams.com

This site uses:

* Gatsby & React as a static site generator
* Contentful as a headless CMS
* Netlify to host the static site

Even though this is a static site the content is still authored in and populated via Contentful's CMS. Using webhooks I'm able to regenerate the static site each time a post is added/updated in Contentful using a webhook to rebuild the static files.


``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:8000
$ gatsby develop

# build for production
$ gatsby build
 
# start local server for build testing
$ gatsby serve
```