module.exports = {
  siteMetadata: {
    title: `Jeremy Abrahams | Developer, UX Strategist, &amp; Accessibility Specialist`
  },
  plugins: [
    {
      resolve: `gatsby-source-contentful`,
      options: {
        spaceId: `baukpwiqm9nw`,
        accessToken: `290430ed7b545dfeb46d856e6185c2fa2c108b94e76753b417d7a653e2809788`
      }
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: "UA-114359409-1",
        head: false // Puts tracking script in the head instead of the body
      }
    },
    {
      resolve: `gatsby-plugin-google-fonts`,
      options: {
        fonts: [
          `roboto\:300,400` // you can also specify font weights and styles
        ]
      }
    },
    `gatsby-transformer-remark`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-plugin-postcss-sass`
    // `gatsby-plugin-sass`,
  ]
};
