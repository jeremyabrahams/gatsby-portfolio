import React from "react"
// import { TypographyStyle } from "react-typography"
import * as PropTypes from "prop-types"
// import typography from "./utils/typography"

const propTypes = {
  headComponents: PropTypes.node.isRequired,
  body: PropTypes.node.isRequired,
  postBodyComponents: PropTypes.node.isRequired,
}

let stylesStr
if (process.env.NODE_ENV === `production`) {
  try {
    stylesStr = require(`!raw-loader!../public/styles.css`)
  } catch (e) {
    console.log(e)
  }
}

class Html extends React.Component {
  render() {
    const { headComponents, body, postBodyComponents } = this.props
    
    let css
    if (process.env.NODE_ENV === `production`) {
      css = (
        <style
          id="gatsby-inlined-css"
          dangerouslySetInnerHTML={{ __html: stylesStr }}
        />
      )
    }
    
    return (
      <html className="no-js" lang="en-us">
        <head>
          {headComponents}

          <meta charset="utf-8" />
          <meta http-equiv="x-ua-compatible" content="ie=edge" />
          <meta name="description" content="Portfolio of Grand Rapids based Developer, UX Strategiest, and Accessibility Specialist Jeremy Abrahams" />
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
          <meta name="robots" content="noindex" />
          
          <title>Jeremy Abrahams | Developer, UX Strategist, &amp; Accessibility Specialist</title>
          {css}
        </head>
        <body>
          <div id="___gatsby" dangerouslySetInnerHTML={{ __html: body }} />
          {postBodyComponents}
          <div className="footer-wrap">
            <footer className="footer">
              <p className="footer__tagline">Portfolio of Jeremy Abrahams</p>
              <ul className="footer__social">
                <li><a href="mailto:jeremyabrahams@gmail.com">Email</a></li>
                <li><a href="https://www.linkedin.com/in/jeremyabrahams">LinkedIn</a></li>
                <li><a href="https://twitter.com/jeremyabrahams">Twitter</a></li>
                <li><a href="https://www.instagram.com/jeremy_abrahams/">Instagram</a></li>
                <li><a href="https://gitlab.com/users/jeremyabrahams/projects">GitLab</a></li>
              </ul>
            </footer>
          </div>
        </body>
      </html>
    )
  }
}

Html.propTypes = propTypes

export default Html
