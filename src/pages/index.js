import React from "react"
import * as PropTypes from "prop-types"
import Markdown from "markdown-to-jsx"
import Img from "gatsby-image"
import Link from "gatsby-link"
// import { rhythm } from "../utils/typography"

const propTypes = {
  data: PropTypes.object.isRequired,
}

const Skill = ({ node }) => (
  <section className={`skill-block skill-block--${node.skillName.toString().toLowerCase().replace(/\s+/g, '-')}`} id={node.skillName.toString().toLowerCase().replace(/\s+/g, '-')}>
    <Img
      className="skill__image" 
      alt={node.skillHeader.description}
      sizes={node.skillHeader.sizes} 
    />
    <h2 className="skill__title">{node.skillName}</h2>
    <p className="skill__intro">{node.skillIntro.skillIntro}</p>
    <div className="skill__list"><Markdown>{node.skillDescription.skillDescription}</Markdown></div>
  </section>
)

class IndexPage extends React.Component {
  render() {
    const intro = this.props.data.allContentfulIntroduction.edges[0].node
    const skills = this.props.data.allContentfulSkills.edges

    console.log(this.props.data)

    return (
      <div className="site-canvas">
        <header className="header">
          <h1 className="header__title">{intro.introTitle}</h1>
          <p className="header__text">
            <span className="salutation">{intro.salutation}</span>
            <span className="body"><Markdown>{intro.intro.intro}</Markdown></span>
          </p>
        </header>

        <nav className="nav">
          <ul className="nav__list">
            {skills.map(({ node }, i) => (
              <li key={node.id}><Link to={'#'+node.skillName.toString().toLowerCase().replace(/\s+/g, '-')}>{node.skillName}</Link></li>
            ))}
          </ul>
        </nav>

        {skills.map(({ node }, i) => (
          <Skill node={node} key={node.id} />
        ))}
      </div>
    )
  }
}

IndexPage.propTypes = propTypes

export default IndexPage

export const pageQuery = graphql`
  query PageQuery {
    allContentfulIntroduction {
      edges {
        node {
          id
          introTitle
          salutation
          intro {
            intro
          }
        }
      }
    }
    allContentfulSkills(sort: { fields: [sortOrder], order: ASC }) {
      edges {
        node {
          id
          skillHeader {
            description
            sizes(maxWidth: 1055, quality: 90) {
              ...GatsbyContentfulSizes_withWebp_noBase64
            }
          }
          skillName
          skillIntro {
            skillIntro
          }
          skillDescription {
            skillDescription
          }
        }
      }
    }
  }
`